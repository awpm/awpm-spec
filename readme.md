# awpm-spec
the specification for the awesome worplace manifesto

## Availabililty
[![npm](https://awpm.gitlab.io/assets/repo-button-npm.svg)](https://www.npmjs.com/package/awpm-spec)
[![git](https://awpm.gitlab.io/assets/repo-button-git.svg)](https://GitLab.com/awpm/awpm-spec)
[![git](https://awpm.gitlab.io/assets/repo-button-mirror.svg)](https://github.com/awpm/awpm-spec)
[![docs](https://awpm.gitlab.io/assets/repo-button-docs.svg)](https://awpm.gitlab.io/awpm-spec/)

## Status for master
[![build status](https://GitLab.com/awpm/awpm-spec/badges/master/build.svg)](https://GitLab.com/awpm/awpm-spec/commits/master)
[![coverage report](https://GitLab.com/awpm/awpm-spec/badges/master/coverage.svg)](https://GitLab.com/awpm/awpm-spec/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/awpm-spec.svg)](https://www.npmjs.com/package/awpm-spec)
[![Dependency Status](https://david-dm.org/awpm/awpm-spec.svg)](https://david-dm.org/awpm/awpm-spec)
[![bitHound Dependencies](https://www.bithound.io/github/awpm/awpm-spec/badges/dependencies.svg)](https://www.bithound.io/github/awpm/awpm-spec/master/dependencies/npm)
[![bitHound Code](https://www.bithound.io/github/awpm/awpm-spec/badges/code.svg)](https://www.bithound.io/github/awpm/awpm-spec)
[![TypeScript](https://img.shields.io/badge/TypeScript-2.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%206.x.x-blue.svg)](https://nodejs.org/dist/latest-v6.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com/)

## Usage
Use TypeScript for best in class instellisense.

For further information read the linked docs at the top of this README.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy.html)

[![repo-footer](https://awpm.gitlab.io/assets/repo-footer.svg)](https://awpm.xyz)
