---
name: Brandfile Spec
---
# Brandfile Specification
This spec defines which common files should be available for a brand identity. It includes samples of a reference design (created by the team at [Stud10.Agency](http://stud10.agency)).

Every company maintains one or more brands. Designwise a single brand should have a well defined scope and a few standard scenarios covered. Lets dive into what those are.

## Folder Structure
> All files for one Brand Identity should be organized in the following folder structure, so tools like [checkspec](https://npmjs.com/package/checkspec) work as intended:

```
mybrandname
|- source
|  |- 
|   
|- web
   |- 
   
```

## Source Files
Source files should always be available as .ai files and include:

Version | File Type Settings | Description
-- | -- | --
squared logo | .ai, normal | This file contains the the logo as a simple squared version

## Web Files
> Web Files, as they are derived from the **vectorized Source Files** should transport their vector nature in to the browser with .svg.

For every brand identity there should be a set of standard svgs available:

### WebFiles for Code Repositories

#### Badges
Badges are svg images that can be used in places like readme's to show an information or a link to something.

All badges of one single brand should have one originating .ai file. The badges should be non responsive to avoid scaling in the markdown readmes.

**Compact Badges**

Version | File Type | Description | fileName
--| -- | --
Badge Git | .svg, non responsive | A badge that shows the logo, the brandname and the word Git
Badge Npm | .svg, non responsive | A badge that shows the logo, the brandname and the word npm
Badge Mirror | .svg, non responsive | A badge that shwos the logo, the brandname and the word Mirror
Badge Docs | .svg, non responsive | A badge that shwos the logo, the brandname and the word Docs

**Less Compact Badges**

Version | File Type | Description
-- | -- | --
Badge Changelog | .svg, nonresponsive | a little bigger badge that links to the changelog of a specific repository
Badge Website | .svg, nonresponsive | a little bigger badge that links to the website of a specific repository
Badge Blogpost | .svg, non-responsive | a little bigger badge that links to an optional blogpost of a respository

### Web Files for Social Media