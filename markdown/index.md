# Awesome Workplace Manifesto
the specification for an awesome workplace

## What is this?
AWPM is the specification of how to design the workplace to be awesome. We do this by defining a set standards that need to be followed in order to qualify as awesome workplace according to this spec.

## Who defines the specification?
They are a community effort and maintained on GitLab. If you have an suggestion feel free to hand in a merge request, so people can discuss about it.
